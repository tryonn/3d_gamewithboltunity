// GENERATED AUTOMATICALLY FROM 'Assets/_Game-TPS/Settings/GameInputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameInputs : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameInputs()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameInputs"",
    ""maps"": [
        {
            ""name"": ""PlayerMovControls"",
            ""id"": ""ebc6f3b3-fe7e-4cee-be45-44dd5acd0ab6"",
            ""actions"": [
                {
                    ""name"": ""move"",
                    ""type"": ""Value"",
                    ""id"": ""49150c0b-beff-48c5-b7be-ed0b867ec1df"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""look"",
                    ""type"": ""Value"",
                    ""id"": ""8688958c-2602-4cb4-9bab-b9f442745d79"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""keyboard"",
                    ""id"": ""f242b22e-c1e0-4ad2-8bff-2c25cb632b81"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""8199996d-3c73-4b7f-a8c2-d762bb727bcb"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""447aeb02-0d2c-46b9-afe3-65afaa9d43ee"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""5e17b543-569e-4a20-b1e5-dc6e6e6a9e50"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e7a7a845-7cd3-4c9a-91af-71ce5b4db706"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""862d935c-8563-48f0-a924-123782b6d382"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cd534d55-b3d4-40fe-8c35-ad73b737fbfc"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""38f0d87f-5255-4856-ad85-a041ce7cd9ce"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PlayerActionsButtons"",
            ""id"": ""b4b21bb6-a079-494f-8ebe-af6d12396719"",
            ""actions"": [
                {
                    ""name"": ""jump"",
                    ""type"": ""PassThrough"",
                    ""id"": ""e2a861ee-be0d-4ec9-b3da-7855d5d8570f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Slide"",
                    ""type"": ""Button"",
                    ""id"": ""c10f6e17-4de4-4958-a0ea-fe0fdabffb88"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7bd300f9-5e9f-4afe-a37e-4eb74a8f1d9d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a9917a1-c1c3-41b9-99e5-d9295813a61d"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""578a3a0d-83f2-40ee-823b-97e95408a2b1"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Slide"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d178e68-0c71-4908-af91-e0299c95a568"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Slide"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerMovControls
        m_PlayerMovControls = asset.FindActionMap("PlayerMovControls", throwIfNotFound: true);
        m_PlayerMovControls_move = m_PlayerMovControls.FindAction("move", throwIfNotFound: true);
        m_PlayerMovControls_look = m_PlayerMovControls.FindAction("look", throwIfNotFound: true);
        // PlayerActionsButtons
        m_PlayerActionsButtons = asset.FindActionMap("PlayerActionsButtons", throwIfNotFound: true);
        m_PlayerActionsButtons_jump = m_PlayerActionsButtons.FindAction("jump", throwIfNotFound: true);
        m_PlayerActionsButtons_Slide = m_PlayerActionsButtons.FindAction("Slide", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerMovControls
    private readonly InputActionMap m_PlayerMovControls;
    private IPlayerMovControlsActions m_PlayerMovControlsActionsCallbackInterface;
    private readonly InputAction m_PlayerMovControls_move;
    private readonly InputAction m_PlayerMovControls_look;
    public struct PlayerMovControlsActions
    {
        private @GameInputs m_Wrapper;
        public PlayerMovControlsActions(@GameInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @move => m_Wrapper.m_PlayerMovControls_move;
        public InputAction @look => m_Wrapper.m_PlayerMovControls_look;
        public InputActionMap Get() { return m_Wrapper.m_PlayerMovControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerMovControlsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerMovControlsActions instance)
        {
            if (m_Wrapper.m_PlayerMovControlsActionsCallbackInterface != null)
            {
                @move.started -= m_Wrapper.m_PlayerMovControlsActionsCallbackInterface.OnMove;
                @move.performed -= m_Wrapper.m_PlayerMovControlsActionsCallbackInterface.OnMove;
                @move.canceled -= m_Wrapper.m_PlayerMovControlsActionsCallbackInterface.OnMove;
                @look.started -= m_Wrapper.m_PlayerMovControlsActionsCallbackInterface.OnLook;
                @look.performed -= m_Wrapper.m_PlayerMovControlsActionsCallbackInterface.OnLook;
                @look.canceled -= m_Wrapper.m_PlayerMovControlsActionsCallbackInterface.OnLook;
            }
            m_Wrapper.m_PlayerMovControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @move.started += instance.OnMove;
                @move.performed += instance.OnMove;
                @move.canceled += instance.OnMove;
                @look.started += instance.OnLook;
                @look.performed += instance.OnLook;
                @look.canceled += instance.OnLook;
            }
        }
    }
    public PlayerMovControlsActions @PlayerMovControls => new PlayerMovControlsActions(this);

    // PlayerActionsButtons
    private readonly InputActionMap m_PlayerActionsButtons;
    private IPlayerActionsButtonsActions m_PlayerActionsButtonsActionsCallbackInterface;
    private readonly InputAction m_PlayerActionsButtons_jump;
    private readonly InputAction m_PlayerActionsButtons_Slide;
    public struct PlayerActionsButtonsActions
    {
        private @GameInputs m_Wrapper;
        public PlayerActionsButtonsActions(@GameInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @jump => m_Wrapper.m_PlayerActionsButtons_jump;
        public InputAction @Slide => m_Wrapper.m_PlayerActionsButtons_Slide;
        public InputActionMap Get() { return m_Wrapper.m_PlayerActionsButtons; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActionsButtonsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActionsButtonsActions instance)
        {
            if (m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface != null)
            {
                @jump.started -= m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface.OnJump;
                @jump.performed -= m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface.OnJump;
                @jump.canceled -= m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface.OnJump;
                @Slide.started -= m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface.OnSlide;
                @Slide.performed -= m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface.OnSlide;
                @Slide.canceled -= m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface.OnSlide;
            }
            m_Wrapper.m_PlayerActionsButtonsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @jump.started += instance.OnJump;
                @jump.performed += instance.OnJump;
                @jump.canceled += instance.OnJump;
                @Slide.started += instance.OnSlide;
                @Slide.performed += instance.OnSlide;
                @Slide.canceled += instance.OnSlide;
            }
        }
    }
    public PlayerActionsButtonsActions @PlayerActionsButtons => new PlayerActionsButtonsActions(this);
    public interface IPlayerMovControlsActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
    }
    public interface IPlayerActionsButtonsActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnSlide(InputAction.CallbackContext context);
    }
}
