// GENERATED AUTOMATICALLY FROM 'Assets/_Game-TPS/Settings/GameInputControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameInputControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameInputControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameInputControls"",
    ""maps"": [
        {
            ""name"": ""PlayerGameControls"",
            ""id"": ""6499cd14-f29c-4d86-9cb9-7b7ac48dc881"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f662e1ba-3cac-45ba-a9e3-fc807c49f03f"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""b12c3314-8621-43ef-bc67-12466b2a23d4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0a39a4c5-ff7e-49b8-8e65-b08a43e1bf5d"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""KeyboardMap"",
                    ""id"": ""857076c7-3ce4-43ba-90c8-b471e2bccbba"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""60368d4e-8533-4962-ac9e-299ea41b9b8a"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0d50beca-188f-4bf7-9684-f85c59939cab"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""dae724f9-186e-4631-93cb-94fd0eebdc2a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f15fe91b-30ea-4c91-a65b-675b9e3ac559"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""514753d1-a283-4b3b-92c1-159f4bce29ae"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e4932aa5-9ae4-43a3-bc4d-e41bfffc0cd5"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerGameControls
        m_PlayerGameControls = asset.FindActionMap("PlayerGameControls", throwIfNotFound: true);
        m_PlayerGameControls_Move = m_PlayerGameControls.FindAction("Move", throwIfNotFound: true);
        m_PlayerGameControls_Jump = m_PlayerGameControls.FindAction("Jump", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerGameControls
    private readonly InputActionMap m_PlayerGameControls;
    private IPlayerGameControlsActions m_PlayerGameControlsActionsCallbackInterface;
    private readonly InputAction m_PlayerGameControls_Move;
    private readonly InputAction m_PlayerGameControls_Jump;
    public struct PlayerGameControlsActions
    {
        private @GameInputControls m_Wrapper;
        public PlayerGameControlsActions(@GameInputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_PlayerGameControls_Move;
        public InputAction @Jump => m_Wrapper.m_PlayerGameControls_Jump;
        public InputActionMap Get() { return m_Wrapper.m_PlayerGameControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerGameControlsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerGameControlsActions instance)
        {
            if (m_Wrapper.m_PlayerGameControlsActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerGameControlsActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerGameControlsActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerGameControlsActionsCallbackInterface.OnMove;
                @Jump.started -= m_Wrapper.m_PlayerGameControlsActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerGameControlsActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerGameControlsActionsCallbackInterface.OnJump;
            }
            m_Wrapper.m_PlayerGameControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
            }
        }
    }
    public PlayerGameControlsActions @PlayerGameControls => new PlayerGameControlsActions(this);
    public interface IPlayerGameControlsActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
    }
}
