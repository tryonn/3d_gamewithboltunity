﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManagerControls : MonoBehaviour
{
    GameInputs _palyerInput;

    public Dictionary<string, Vector2> _vectorInputs = new Dictionary<string, Vector2>();
    public Dictionary<string, bool> _onButtonHold = new Dictionary<string, bool>();
    public Dictionary<string, bool> _onButtonDown = new Dictionary<string, bool>();
    public Dictionary<string, bool> _onButtonUp = new Dictionary<string, bool>();


    private void Awake()
    {
        _palyerInput = new GameInputs();
    }
    
    // Start is called before the first frame update
    void Start()
    {
         foreach (var inputVector2 in _palyerInput.PlayerMovControls.Get().actions)
        {
            _vectorInputs.Add(inputVector2.name, Vector2.zero);
        }

        foreach (var inputButton in _palyerInput.PlayerActionsButtons.Get().actions)
        {
            _onButtonHold.Add(inputButton.name, false);
            _onButtonDown.Add(inputButton.name, false);
            _onButtonUp.Add(inputButton.name, false);

            inputButton.performed += delegate { _onButtonHold[inputButton.name] = true; };
            inputButton.canceled += delegate { _onButtonHold[inputButton.name] = false; };
            inputButton.canceled += delegate { _onButtonUp[inputButton.name] = false; };
        }        
    }

    // Update is called once per frame
    void Update()
    {
         foreach (var buttonpress in _palyerInput.PlayerActionsButtons.Get().actions)
        {
            _onButtonDown[buttonpress.name] = buttonpress.triggered;
        }

        foreach (var buttonpress in _palyerInput.PlayerActionsButtons.Get().actions)
        {
            _onButtonUp[buttonpress.name] = false;
        }

        foreach (var vectorInputs in _palyerInput.PlayerMovControls.Get().actions)
        {
            _vectorInputs[vectorInputs.name] = vectorInputs.ReadValue<Vector2>();
        }
    }

    private void OnEnable()
    {
        _palyerInput.Enable();
    }

    private void OnDisable()
    {
        _palyerInput.Disable();
    }
}
