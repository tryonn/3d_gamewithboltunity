using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewInputInterface : MonoBehaviour
{
    GameInputControls control;

   // public Vector2 leftAnalog;

    public Dictionary<string, Vector2> _vectorInputs = new Dictionary<string, Vector2>();

    public Dictionary<string, bool> onButtonHold = new Dictionary<string, bool>();

    public Dictionary<string, bool> onButtonDown = new Dictionary<string, bool>();

    public Dictionary<string, bool> onButtonUp = new Dictionary<string, bool>();


    private void Awake()
    {
        control = new GameInputControls();
    }

    private void OnEnable()
    {
        control.Enable();
    }
    private void OnDisable()
    {
        control.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
        foreach (var item in control.PlayerGameControls.Get().actions)
        {
            _vectorInputs.Add(item.name, Vector2.zero);

            onButtonHold.Add(item.name, false);
            onButtonDown.Add(item.name, false);
            onButtonUp.Add(item.name, false);

            item.performed += delegate { onButtonHold[item.name] = true; };
            item.performed += delegate { onButtonHold[item.name] = false; };

            item.performed += delegate { onButtonUp[item.name] = false; };

        }
    }

    // Update is called once per frame
    void Update()
    {
       // leftAnalog = control.PlayerGameControls.Move.ReadValue<Vector2>();
        foreach (var item in control.PlayerGameControls.Get().actions)
        {
            onButtonDown[item.name] = item.triggered;

            _vectorInputs[item.name] = item.ReadValue<Vector2>();
        }
    }

    private void LateUpdate()
    {
        foreach (var item in control.PlayerGameControls.Get().actions)
        {
            onButtonUp[item.name] = false;
        }
    }
}
