# 3D_GameWithBoltUnity

Projeto utilizando Unity e Bolt, mecânicas 2.5D, TPS e FPS


 # Assets Utilizados no projeto
  
  - Basic Motions FREE Pack
  		-- https://assetstore.unity.com/packages/3d/animations/basic-motions-free-pack-154271

  - Dungeon - Low Poly Toon Battle Arena / Tower Defense Pack
  		-- https://assetstore.unity.com/packages/3d/environments/dungeons/dungeon-low-poly-toon-battle-arena-tower-defense-pack-109791

